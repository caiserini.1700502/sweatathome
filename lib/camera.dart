import 'dart:async';

import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:tflite/tflite.dart';

typedef void Callback(List<dynamic> list, int h, int w);

class Camera extends StatefulWidget {
  final Function passRecognition;
  Camera(this.passRecognition);

  @override
  _CameraState createState() => new _CameraState();
}

class _CameraState extends State<Camera> {
  CameraController? controller;
  List<CameraDescription> cameras = [];
  bool isDetecting = false;
  int camera = 0;

  @override
  void initState() {
    super.initState();
    startCameraStream();
  }

  startCameraStream() async {
    String? res = await Tflite.loadModel(
      model: "assets/model/posenet.tflite",
      numThreads: 4,
      isAsset: true,
      useGpuDelegate: true,
    );

    try {
      cameras = await availableCameras();
      print(cameras);
    } on CameraException catch (e) {
      print('Error: $e.code\nError Message: $e.message');
    }

    controller = new CameraController(
      cameras[camera],
      ResolutionPreset.veryHigh,
      enableAudio: false,
    );

    await controller!.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });

    controller!.startImageStream((CameraImage img) {
      if (!isDetecting) {
        isDetecting = true;
        poseNetInference(img);
      }
    });
  }

  Future<Null> poseNetInference(CameraImage img) async {
    Tflite.runPoseNetOnFrame(
      bytesList: img.planes.map((p) {
        return p.bytes;
      }).toList(),
      imageHeight: img.height,
      imageWidth: img.width,
      numResults: 2,
      rotation: cameras[camera].sensorOrientation,
      asynch: true,
    ).then((recognitions) {
      //widget.setRecognitions(recognitions, img.height, img.width);
      widget.passRecognition(recognitions);
      isDetecting = false;
    });
  }

  switchCamera() {
    camera = (camera + 1) % 2;
    startCameraStream();
    setState(() {});
  }

  @override
  void dispose() {
    controller!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (controller == null || !controller!.value.isInitialized) {
      return Container();
    }

    return Stack(
      //fit: StackFit.loose,
      alignment: AlignmentDirectional.topStart,
      children: <Widget>[
        CameraPreview(controller!),
        Padding(
          padding: const EdgeInsets.all(25),
          child: Align(
            alignment: Alignment.bottomLeft,
            child: RawMaterialButton(
              onPressed: () => {switchCamera()},
              elevation: 2.0,
              fillColor: Theme.of(context).primaryIconTheme.color,
              child: Icon(
                Icons.loop,
                size: 35.0,
                color: Colors.white60,
              ),
              padding: EdgeInsets.all(10.0),
              shape: CircleBorder(),
            ),
          ),
        )
      ],
    );
  }
}
