import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sweatathome/user.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

final gender = ["Male", "Female", "Non-binary", "Other"];

class _RegisterState extends State<Register> {
  final formKey = new GlobalKey<FormBuilderState>();

  Profile new_user = Profile.empty();

  @override
  Widget build(BuildContext context) {
    String? display_name = auth.FirebaseAuth.instance.currentUser!.displayName;
    List<String> given_name =
        display_name != null ? display_name.split(" ") : [];
    final nameField = FormBuilderTextField(
      name: "name",
      autofocus: given_name.length == 0,
      initialValue: given_name.elementAt(0),
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(context),
        FormBuilderValidators.match(context, "[a-zA-Z ]+"),
      ]),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onSaved: (value) => new_user.name = value!.trim(),
      decoration: InputDecoration(
        labelText: "Enter your name",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        prefixIcon: Icon(
          Icons.person,
        ),
      ),
      maxLines: 1,
    );

    final surnameField = FormBuilderTextField(
      name: "surname",
      initialValue: given_name.elementAt(1),
      autofocus: given_name.length == 1,
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(context),
        FormBuilderValidators.match(context, "[a-zA-Z ]+"),
      ]),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onSaved: (value) => new_user.surname = value!.trim(),
      decoration: InputDecoration(
        labelText: "Enter your last name",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        prefixIcon: Icon(
          Icons.person,
        ),
      ),
      maxLines: 1,
    );

    final usernameField = FormBuilderTextField(
      name: "username",
      enabled: false,
      onSaved: (value) => new_user.username = value!,
      initialValue: display_name,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        prefixIcon: Icon(
          Icons.person,
        ),
      ),
    );

    final genderField = FormBuilderDropdown(
      name: "Gender",
      allowClear: true,
      items: gender
          .map(
            (g) => DropdownMenuItem(
              child: Text('$g'),
              value: g.toString(),
            ),
          )
          .toList(),
      validator: FormBuilderValidators.required(context),
      autovalidateMode: AutovalidateMode.always,
      onSaved: (value) => new_user.gender = value! as String,
      decoration: InputDecoration(
        labelText: "Gender",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        prefixIcon: Icon(
          Icons.person,
        ),
      ),
    );

    final heightField = TextFormField(
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(context),
        FormBuilderValidators.numeric(context),
      ]),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: given_name.length >= 2,
      keyboardType: TextInputType.number,
      onSaved: (value) => new_user.height = double.parse(value!),
      decoration: InputDecoration(
        labelText: "Enter your height",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        prefixIcon: Icon(
          Icons.height,
        ),
      ),
    );

    final weightField = TextFormField(
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(context),
        FormBuilderValidators.numeric(context),
      ]),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.number,
      onSaved: (value) => new_user.weight = double.parse(value!),
      decoration: InputDecoration(
        labelText: "Enter your weight",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        prefixIcon: Icon(
          Icons.circle,
        ),
      ),
    );

    return Scaffold(
        appBar: AppBar(
          title: Text("Registration",
              style: TextStyle(fontWeight: FontWeight.w600)),
          centerTitle: true,
        ),
        resizeToAvoidBottomInset: true,
        body: FormBuilder(
          key: formKey,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(8.0),
            primary: true,
            child: Column(
              children: [
                //usernameField,
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: genderField,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: nameField,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: surnameField,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: heightField,
                ),
                weightField,
                ElevatedButton(
                  onPressed: () {
                    formKey.currentState!.save();
                    if (formKey.currentState!.validate()) {
                      Navigator.popAndPushNamed(context, "/home",
                          arguments: new_user.save_user());
                    }
                  },
                  child: Text("Submit"),
                )
              ],
            ),
          ),
        ));
  }
}
