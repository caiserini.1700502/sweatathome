import 'package:flutter/material.dart';

class WatchEx extends StatelessWidget {
  WatchEx();

  List<double> workouts = [
    0.3,
    0.7,
    0.9,
    0.8,
    0.5,
    0.7,
    0.7,
    0.9,
    0.8,
    0.5,
    0.7,
    0.7,
    0.9,
    0.8,
    0.5,
    0.7,
  ];

  List<String> dates = [
    '15/10/21',
    '16/10/21',
    '17/10/21',
    '18/10/21',
    '19/10/21',
    '21/10/21',
    '22/10/21',
    '23/10/21',
    '24/10/21',
    '25/10/21',
    '26/10/21',
    '27/10/21',
    '29/10/21',
    '30/10/21',
    '01/10/21',
    '02/10/21',
  ];
  Widget view_list(BuildContext context, int i) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          'Workout ${i + 1}: ',
          overflow: TextOverflow.visible,
          style: TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 18,
          ),
        ),
        Row(
          children: <Widget>[
            Text(
              '${(workouts[i] * 100).round()}/100',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 21,
                //color: scColor.transform(scores[i]),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
            ),
            Text(
              '${(dates[i])}',
              style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic),
            ),
          ],
        ),
      ],
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Summary', style: TextStyle(fontWeight: FontWeight.w600)),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 21, horizontal: 18),
            child: Text(
              'Here are your recent workouts with your obtained score.',
              style: TextStyle(
                fontSize: 21,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.symmetric(horizontal: 18, vertical: 10),
              physics: AlwaysScrollableScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              primary: true,
              itemBuilder: view_list,
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(
                thickness: 1.3,
              ),
              itemCount: workouts.length,
            ),
          ),
        ],
      ),
    );
  }
}
