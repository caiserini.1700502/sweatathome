class Args {}

class ArgsBeginEx extends Args {
  final String exname;
  String prefix;
  ArgsBeginEx(this.exname, {this.prefix = 'assets/exercises'});
}

class ArgsRouteEx extends Args {
  final String exname;
  ArgsRouteEx(this.exname);
}
