import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:wakelock/wakelock.dart';
import 'dart:convert';

import 'argparser.dart';
import 'exutils.dart';
import 'camera.dart';
import 'bndbox.dart';
import 'picovoice.dart' as pcv;

class Beginexercise extends StatefulWidget {
  Beginexercise();

  @override
  _BeginexerciseState createState() => new _BeginexerciseState();
}

class _BeginexerciseState extends State<Beginexercise> {
  List<dynamic> _recognitions = [];
  List<dynamic> saved_res = [];
  int _imageHeight = 0;
  int _imageWidth = 0;
  bool record = false;
  String exercisename = "";

  setRecognitions(recognitions) async {
    if (record) {
      saved_res.addAll(recognitions);
      print('Number of poses found: ${saved_res.length}');
    }
    setState(() {
      _recognitions = recognitions;
    });
  }

  String convLinktoMap(dynamic link) {
    if (!(link is Map)) {
      return link.toString();
    }
    HashMap<String, String> map = HashMap();
    for (var e in link.entries) {
      map[e.key.toString()] = convLinktoMap(e.value);
    }
    return jsonEncode(map);
  }

  String stopRecording() {
    record = false;

    List<String> mapRes = [];
    for (var res in saved_res) {
      res["keypoints"].removeWhere((k, v) => {
            "nose",
            "leftEye",
            "rightEye",
            "leftEar",
            "rightEar"
          }.contains(v["part"]));
      transform(res);
      scale(res);
      mapRes.add(convLinktoMap(res));
    }

    String source = jsonEncode(mapRes);
    print('--------------------------------Done Encoding -------------------');
    writeToFile(
      exercisename + '.json',
      source,
    );
    print('--------------------------------Done Writing -------------------');
    Navigator.pop(context);
    return "i'm stopping";
  }

  @override
  void initState() {
    super.initState();
    pcv.start(callbacks: {
      "Recording": () {
        record = true;
        return "I'm recording";
      },
      "Stop": stopRecording
    });
    Wakelock.enable();
  }

  @override
  void dispose() {
    Wakelock.disable();
    pcv.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    ArgsBeginEx args =
        ModalRoute.of(context)!.settings.arguments as ArgsBeginEx;
    exercisename = args.exname;
    return Scaffold(
        appBar: AppBar(
          title:
              Text(exercisename, style: TextStyle(fontWeight: FontWeight.w600)),
          centerTitle: true,
          leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.pop(context)),
        ),
        body: Stack(
          children: <Widget>[
            Camera(setRecognitions),
            BndBox(
              _recognitions,
              1920,
              1080,
              screen,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: RaisedButton(
                  padding:
                      EdgeInsets.symmetric(vertical: 7.0, horizontal: 14.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(37.0),
                    side: BorderSide(
                      color: Colors.grey,
                    ),
                  ),
                  color: Theme.of(context).primaryIconTheme.color,
                  elevation: 5.0,
                  child: Text(
                    !record ? 'Start' : 'Stop',
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.white60,
                      fontSize: 42,
                    ),
                  ),
                  onPressed: !record ? () => record = true : stopRecording,
                ),
              ),
            )
          ],
        ));
  }
}
