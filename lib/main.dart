import 'dart:io';

import 'package:async_builder/init_builder.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sweatathome/picovoice.dart';
import 'package:sweatathome/user.dart' as us;
import 'package:sweatathome/watch_ex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'homepage.dart';
import 'beginexercise.dart';
import 'routeexercise.dart';
import 'video.dart';
import 'logging.dart';
import 'watch_ex.dart';
import 'register.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:async_builder/async_builder.dart';
import 'package:async_builder/init_builder.dart';

Directory? app_dir;
//List<CameraDescription> cameras;
main() async {
  WidgetsFlutterBinding.ensureInitialized();
  initTTS();
  app_dir = await getApplicationDocumentsDirectory();
  runApp(SweatAtHome());
}

bool first_login = false;

class SweatAtHome extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Swe@Home',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.blueGrey,
          accentColor: Colors.blue,
        ),
        primaryIconTheme: IconThemeData(
          color: Colors.blueGrey[300],
        ),
        fontFamily: 'Raleway',
      ),
      routes: <String, WidgetBuilder>{
        '/': (BuildContext context) => LoggingState(),
        '/logging': (BuildContext context) => LoggingPage(),
        '/home': (BuildContext context) => Homepage(),
        '/exercise': (BuildContext context) => Routexercise(),
        '/recordex': (BuildContext context) => Beginexercise(),
        '/videoex': (BuildContext context) => StartExercise(),
        '/watchex': (BuildContext context) => WatchEx(),
        '/register': (BuildContext context) => Register(),
      },
    );
  }
}

class LoggingState extends StatefulWidget {
  const LoggingState({Key? key}) : super(key: key);

  @override
  _LoggingStateState createState() => _LoggingStateState();
}

class _LoggingStateState extends State<LoggingState> {
  static Future<FirebaseApp> startFirebase() async => Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return InitBuilder(
      getter: startFirebase,
      builder: (context, Future<Object>? future) => AsyncBuilder(
        future: future,
        builder: (context, value) {
          FirebaseAuth.instance.authStateChanges().listen((User? user) {
            if (user == null) {
              Navigator.pushNamedAndRemoveUntil(
                  context, "/logging", ModalRoute.withName("/"));
            } else {
              Navigator.pushNamedAndRemoveUntil(
                context,
                "/home",
                ModalRoute.withName("/"),
                arguments: us.load_user(),
              );
            }
          });
          return LoggingPage();
        },
        waiting: (context) => LoggingLoading(),
        error: (context, error, stackTrace) => LoggingError(),
      ),
    );
  }
}
