import 'package:flutter/material.dart';

class EndExercise extends StatelessWidget {
  final List<double> scores;
  final String name;

  EndExercise(this.scores, this.name);

  int avgscore() {
    double res = scores.reduce((a, b) => a + b) * 100 / scores.length;
    return res.round();
  }

  final ColorTween scColor =
      ColorTween(begin: Colors.red, end: Colors.greenAccent[700]);

  Widget view_list(BuildContext context, int i) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          'Exercise ${i + 1} : ',
          style: TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 26,
          ),
        ),
        Text(
          '${(scores[i] * 100).round()}/100',
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 26,
            color: scColor.transform(scores[i]),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Finish', style: TextStyle(fontWeight: FontWeight.w600)),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 18),
            child: Text(
              'Congratulations! You\'re done!',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.symmetric(horizontal: 18, vertical: 10),
              physics: AlwaysScrollableScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              primary: true,
              itemBuilder: view_list,
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(
                thickness: 0.9,
              ),
              itemCount: scores.length,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 20, 15, 30),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Text(
                    'Your average score is: ',
                    overflow: TextOverflow.clip,
                    style: TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  '${avgscore()}/100',
                  overflow: TextOverflow.clip,
                  style: TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.bold,
                      color: scColor.transform(avgscore() / 100)),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
