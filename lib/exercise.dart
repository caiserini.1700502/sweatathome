import 'dart:collection';
import 'dart:math' as math;
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

import 'exutils.dart';

class Exercise {
  List<dynamic> saved_exercise = [];
  List<dynamic> start_seq = [];
  List<dynamic> end_seq = [];
  ExState state = ExState.Beginning;
  double th_execution = 0.1, th_start = 0.71, th_finish = 0.80;
  int ignore = 20;
  int cnt_ignore = 0;
  List<dynamic> poses = [];
  Function(double, int)? setScore;
  int num_execution = 0;

  Exercise(
    String prefix,
    String ex,
    Function(double, int) funscore,
  ) {
    //load from file the right exercise and save them in saved_exercise
    setScore = funscore;
    loadExercise("$prefix/$ex.json").then((lst) {
      saved_exercise = lst;
      int perc_ex = math.max((saved_exercise.length / 20).floor(), 5);
      start_seq = saved_exercise.sublist(0, perc_ex);
      end_seq = saved_exercise.sublist(saved_exercise.length - perc_ex);
      ignore = (saved_exercise.length / 10).round();
    });
  }

  void getScore() {
    double score = 0.0;
    print("${saved_exercise.length}  ---- ${poses.length}");
    Iterator<dynamic> itposes = poses.iterator;
    if (!itposes.moveNext() || itposes.current == null) {
      print("no body was found");
      return;
    }
    int count = 0;
    for (var saved in saved_exercise) {
      double score_single = 0.0;
      double cmp = 0.0;
      do {
        if (itposes.current == null) {
          break;
        }
        cmp = diffPose(saved, itposes.current);
        if (score_single < cmp) {
          score_single = cmp;
        } else {
          break;
        }
        count += 1;
        score += score_single;
      } while (itposes.moveNext() || itposes.current == null);
    }
    score /= count;
    num_execution += 1;
    setScore!(math.min(score + th_execution, 1.0), num_execution);
    poses.clear();
  }

  Future<void> addPose(List<dynamic> results) async {
    //results.removeWhere((element) => element == null);
    if (results.isEmpty || results.length < 1) {
      return;
    }

    for (var i = 0; i < results.length; i++) {
      results[i]["keypoints"].removeWhere((k, v) => {
            "nose",
            "leftEye",
            "rightEye",
            "leftEar",
            "rightEar"
          }.contains(v["part"]));
      transform(results[i]);
      scale(results[i]);
    }
    double sc = 0;
    switch (state) {
      case ExState.Beginning:
        sc = window_diffPoses(start_seq, results);
        if (sc > th_start) {
          state = ExState.Starting;
        }
        break;
      case ExState.Starting:
        sc = window_diffPoses(start_seq, results);
        if (sc < th_start) {
          state = ExState.InProgress;
        }
        break;
      case ExState.InProgress:
        if (cnt_ignore > ignore) {
          sc = window_diffPoses(end_seq, results);
          if (sc > th_finish) {
            getScore();
            state = ExState.Beginning;
            cnt_ignore = 0;
          }
        } else {
          cnt_ignore += 1;
        }
        break;
    }

    // double sc = diffPoses(saved_exercise.first, results);
    // if (sc < th_start) {
    //   if (state == ExState.Starting) {
    //     state = ExState.InProgress;
    //   }
    // } else {
    //   if (state == ExState.InProgress) {
    //     getScore();
    //     state = ExState.Starting;
    //   }
    // }
    if (state != ExState.Beginning) {
      poses.addAll(results);
    }
    print(
        "------------------------------------------------ $state : ${(sc * 100).round()}/${state == ExState.InProgress ? th_finish : th_start} : ${poses.length}");
  }

  Future<List> loadExercise(String file) async {
    String ex_s = await loadAsset(file);
    List lst = jsonDecode(ex_s);
    for (int i = 0; i < lst.length; i++) {
      lst[i] = decodePoses(lst[i]); //jsonDecode(lst[i]);
    }
    print(
        "Loaded type : ${lst.runtimeType} Elem type: ${lst.first.runtimeType}");
    return lst;
  }

  decodePoses(dynamic m) {
    LinkedHashMap<String, dynamic> map = jsonDecode(m);
    map["stats"] = jsonDecode(map["stats"]);
    LinkedHashMap<dynamic, dynamic> kpoint =
        jsonDecode(map["keypoints"]).map((k, v) {
      return MapEntry(int.parse(k), v);
    });
    map["keypoints"] = kpoint;
    for (var e in map["stats"].entries) {
      map["stats"][e.key] = jsonDecode(e.value);
    }
    for (var e in map["keypoints"].entries) {
      map["keypoints"][e.key] = jsonDecode(e.value);
      map["keypoints"][e.key]["x"] = double.parse(map["keypoints"][e.key]["x"]);
      map["keypoints"][e.key]["y"] = double.parse(map["keypoints"][e.key]["y"]);
      // for (var ee in map["keypoints"][e.key].entries) {
      //   map["keypoints"][e.key][ee.key] = jsonDecode(ee.value);
      // }
    }

    return map;
  }

  Future<String> loadAsset(String file) async {
    String s = await rootBundle.loadString(file);
    return s;
  }
}

enum ExState {
  Starting,
  InProgress,
  Beginning,
}
