import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sweatathome/main.dart';
import 'user.dart' as u;

class LoggingError extends StatelessWidget {
  const LoggingError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Swe@Home", style: TextStyle(fontWeight: FontWeight.w600)),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: [
            Text(
              "Sorry! 😞",
              style: TextStyle(fontSize: 24),
            ),
            Text(
                "It seems something went wrong while connecting to the database."),
            ElevatedButton(
                onPressed: () => Navigator.popAndPushNamed(context, '/'),
                child: Text("Try Again!")),
          ],
        ),
      ),
    );
  }
}

class LoggingLoading extends StatelessWidget {
  const LoggingLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Swe@Home", style: TextStyle(fontWeight: FontWeight.w600)),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: [
            SpinKitCircle(
              color: Theme.of(context).buttonColor,
              duration: Duration(seconds: 2),
              size: 40.0,
            ),
            Text(
              "Connecting...",
              style: TextStyle(fontSize: 24),
            )
          ],
        ),
      ),
    );
  }
}

class LoggingPage extends StatelessWidget {
  const LoggingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Swe@Home", style: TextStyle(fontWeight: FontWeight.w600)),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Image.asset(
                  "assets/images/primapagina.png",
                ),
              ),
              flex: 4),
          Expanded(
            child: Column(
              children: [
                Text(
                  "Welcome in Swe@Home!",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Padding(padding: const EdgeInsets.only(top: 6)),
                Text(
                  "Sign in to start your experience with us",
                  style: TextStyle(fontSize: 18),
                ),
              ],
            ),
            flex: 1,
          ),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: MaterialButton(
                    onPressed: () => signInWithGoogle(context),
                    padding: EdgeInsets.all(4.0),
                    color: Colors.blueAccent[200],
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          "assets/images/google.png",
                          width: 25,
                          height: 25,
                        ),
                        Text(
                          "Sign in with Google",
                          style: TextStyle(fontSize: 22, color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

void signInWithGoogle(BuildContext context) async {
  if (FirebaseAuth.instance.currentUser != null) {
    Navigator.popAndPushNamed(context, "/home");
  }
  // Trigger the authentication flow
  final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

  // Obtain the auth details from the request
  final GoogleSignInAuthentication googleAuth =
      await googleUser!.authentication;

  // Create a new credential
  final credential = GoogleAuthProvider.credential(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );

  UserCredential user_credential =
      await FirebaseAuth.instance.signInWithCredential(credential);
  /* var has_data = await FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .get(); */
  /* if (user_credential.additionalUserInfo!.isNewUser /* || !has_data.exists */) {
    Navigator.pushNamed(context, "/register");
  } else {
    Navigator.popAndPushNamed(context, "/home");
  } */
  return;
}
