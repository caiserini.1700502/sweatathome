import 'package:flutter/material.dart';

class LanguagePopup extends StatefulWidget {
  @override
  LanguagePopupState createState() => LanguagePopupState();
}

class LanguagePopupState extends State<LanguagePopup> {
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      tooltip: "Choose a language",
      padding: EdgeInsets.zero,
      itemBuilder: (context) {
        return [
          CheckedPopupMenuItem(
            child: Text(
              "English",
              style: TextStyle(color: Colors.black),
            ),
            value: 2,
            checked: true,
          ),
        ];
      },
      child: ButtonTheme(
        minWidth: 130.0,
        height: 100.0,
        padding: EdgeInsets.all(10.0),
        child: RaisedButton(
          color: Colors.white.withOpacity(0.7),
          onPressed: () {},
          // To do that i must call a sort of creation of the object, or I don't know
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Column(
            children: <Widget>[
              Icon(Icons.language, color: Colors.black, size: 40.0),
              Text("Language",
                  style: TextStyle(color: Colors.black, fontSize: 20.0))
            ],
          ),
        ),
      ),
    );
  }
}
