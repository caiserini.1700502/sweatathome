import 'package:flutter/material.dart';

class CustomChoiceChip extends StatefulWidget {
  @override
  ChoiceChipState createState() => ChoiceChipState();
}

class ChoiceChipState extends State<CustomChoiceChip> {
  int indexSelected = -1;

  void _reset() {
    indexSelected = -1;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Wrap(
        children: [
          ChoiceChip(
            labelStyle: TextStyle(
                color: Colors.grey[850], fontSize: 18, fontFamily: 'Raleway'),
            label: Text("Male"),
            //backgroundColor: Colors.grey[120],
            elevation: 5.0,
            selected: indexSelected == 0,
            selectedColor: Colors.blue[200],
            onSelected: (value) {
              setState(() {
                indexSelected = value ? 0 : -1;
              });
            },
          ),
          const SizedBox(width: 8),
          ChoiceChip(
            label: Text("Female"),
            labelStyle: TextStyle(
                color: Colors.grey[850], fontSize: 18, fontFamily: 'Raleway'),
            backgroundColor: Colors.grey[120],
            elevation: 5.0,
            selected: indexSelected == 1,
            selectedColor: Colors.red[300],
            onSelected: (value) {
              setState(() {
                indexSelected = value ? 1 : -1;
              });
            },
          ),
        ],
      ),
    );
  }
}
