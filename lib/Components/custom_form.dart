import 'package:flutter/material.dart';
import 'choice_chip.dart';

class CustomForm extends StatefulWidget {
  @override
  CustomFormState createState() => CustomFormState();
}

// Create a corresponding State class.
// This class holds data related to the form.
class CustomFormState extends State<CustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  final _controller2 = TextEditingController();
  final _controller3 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 5),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 15, 15),
                child: Text(
                  "Enter your gender",
                  style: TextStyle(color: Colors.grey[850], fontSize: 18),
                ),
              ),
              CustomChoiceChip(),
            ],
          ),
          SizedBox(height: 20),
          TextFormField(
            controller: _controller2,
            decoration: InputDecoration(
              labelText: "Enter your height",
              labelStyle: TextStyle(color: Colors.grey[850], fontSize: 18),
              fillColor: Colors.white,
              border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(25.0),
                borderSide: new BorderSide(),
              ),
              suffixIcon: IconButton(
                onPressed: () => _controller2.clear(),
                icon: Icon(Icons.clear),
              ),
            ),
            validator: (value) {
              if (value == null ||
                  value.isEmpty ||
                  !(int.parse(value) is int)) {
                return 'Please enter your height in centimeters';
              }
              return null;
            },
          ),
          SizedBox(height: 20),
          TextFormField(
            controller: _controller3,
            decoration: InputDecoration(
              labelText: "Enter your weight",
              labelStyle: TextStyle(color: Colors.grey[850], fontSize: 18),
              fillColor: Colors.white,
              border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(25.0),
                borderSide: new BorderSide(),
              ),
              suffixIcon: IconButton(
                onPressed: () => _controller3.clear(),
                icon: Icon(Icons.clear),
              ),
            ),
            validator: (value) {
              if (value == null ||
                  value.isEmpty ||
                  !(int.parse(value) is int)) {
                return 'Please enter your weight in kilograms';
              }
              return null;
            },
          ),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 13.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    /*RaisedButton(
                        color: Colors.white.withOpacity(0.7),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                        onPressed: () {
                          // Validate returns true if the form is valid, or false
                          // otherwise.
                          if (_formKey.currentState.validate()) {
                            // Clear all input fields
                            _controller2.clear();
                            _controller3.clear();

                            // If the form is valid, display a Snackbar.
                            ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(content: Text('Change applied')));

                            Future.delayed(Duration(seconds: 1),
                                () => Navigator.pop(context));
                          }
                        },
                        child: Text('Submit',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15.0)),
                      )*/
                    RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(37.0),
                          side: BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                        elevation: 5.0,
                        child: Text(
                          'Submit',
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 20,
                          ),
                        ),
                        onPressed: () {
                          // Validate returns true if the form is valid, or false
                          // otherwise.
                          if (_formKey.currentState!.validate()) {
                            // Clear all input fields
                            _controller2.clear();
                            _controller3.clear();

                            // If the form is valid, display a Snackbar.
                            ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(content: Text('Change applied')));

                            Future.delayed(Duration(seconds: 1),
                                () => Navigator.pop(context));
                          }
                        })
                  ])),
        ],
      ),
    );
  }
}
