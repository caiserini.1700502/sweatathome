import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sweatathome/main.dart';

import '../argparser.dart';

class CustomCard extends StatelessWidget {
  final String title;
  final String content;
  final String img;
  final String text_button;
  final String route;
  final Args? args;

  const CustomCard({
    Key? key,
    required this.title,
    required this.content,
    required this.text_button,
    required this.img,
    required this.route,
    this.args,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(
        vertical: 12,
        horizontal: 10,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(37.0),
      ),
      child: Container(
        constraints: BoxConstraints.tightForFinite(
          //height: 150,
          width: double.maxFinite,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(37.0),
          image: DecorationImage(
            image: AssetImage(img),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
                //fontStyle: FontStyle.italic
              ),
              textAlign: TextAlign.center,
            ),
            Text(
              content,
              style: TextStyle(color: Colors.grey[850], fontSize: 18),
              overflow: TextOverflow.clip,
              textAlign: TextAlign.center,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(37.0),
                  side: BorderSide(
                    color: Colors.grey,
                  ),
                ),
                elevation: 5.0,
                child: Text(
                  text_button,
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                    fontSize: 15,
                  ),
                ),
                onPressed: () =>
                    Navigator.pushNamed(context, route, arguments: args),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ExCard extends StatefulWidget {
  final String title;
  final String content;
  final String img;
  final String text_button;
  final String route;

  ExCard({
    Key? key,
    required this.title,
    required this.content,
    required this.text_button,
    required this.img,
    required this.route,
  }) : super(key: key);

  @override
  _ExCardState createState() => _ExCardState();
}

class _ExCardState extends State<ExCard> {
  bool _show_option = false;

  List<Widget> loadSavedExercise(BuildContext context) {
    List<Widget> lstButton = [];
    for (var f in ['Squat']) {
      String ex = f; //f.split('/')[-1].replaceAll('.json', '');
      lstButton.add(
        TextButton(
          style: TextButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            backgroundColor: Theme.of(context).primaryColor.withOpacity(0.75),
            primary: Colors.blueGrey[400]!.withOpacity(0.75),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
                side: BorderSide(color: Colors.black, width: 0.6)),
          ),
          child: Text(
            ex,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          onPressed: () {
            _show_option = false;
            Navigator.pushNamed(context, widget.route,
                arguments: ArgsBeginEx(ex.toLowerCase()));
          },
        ),
      );
    }
    for (var f in app_dir!.listSync(recursive: false, followLinks: false)) {
      if (f is Directory) {
        continue;
      }
      if (!f.path.endsWith("json")) {
        continue;
      }
      List<String> path = f.path.split('/');
      String name = path.removeLast().split('.')[0];
      lstButton.add(
        TextButton(
          style: TextButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            backgroundColor: Theme.of(context).primaryColor.withOpacity(0.75),
            primary: Colors.blueGrey[400]!.withOpacity(0.75),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
                side: BorderSide(color: Colors.black, width: 0.6)),
          ),
          child: Text(
            name,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          onPressed: () {
            _show_option = false;
            Navigator.pushNamed(context, widget.route,
                arguments: ArgsBeginEx(
                  name.toLowerCase(),
                  prefix: path.join('/'),
                ));
          },
        ),
      );
    }
    return lstButton;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(
        vertical: 12,
        horizontal: 10,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(37.0),
      ),
      child: Container(
        constraints: BoxConstraints.tightForFinite(
          width: double.maxFinite,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(37.0),
          image: DecorationImage(
            image: AssetImage(widget.img),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              widget.title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
              ),
              textAlign: TextAlign.center,
            ),
            Text(
              widget.content,
              style: TextStyle(color: Colors.grey[850], fontSize: 18),
              overflow: TextOverflow.clip,
              textAlign: TextAlign.center,
            ),
            AnimatedCrossFade(
                alignment: Alignment.bottomCenter,
                firstChild: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(37.0),
                    side: BorderSide(
                      color: Colors.grey,
                    ),
                  ),
                  elevation: 5.0,
                  child: Text(
                    widget.text_button,
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      _show_option = true;
                    });
                  },
                ),
                secondChild: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: loadSavedExercise(context),
                ),
                crossFadeState: !_show_option
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: const Duration(seconds: 1)),
          ],
        ),
      ),
    );
  }
}

class RecCard extends StatefulWidget {
  final String title;
  final String content;
  final String img;
  final String text_button;
  final String route;

  RecCard({
    Key? key,
    required this.title,
    required this.content,
    required this.text_button,
    required this.img,
    required this.route,
  }) : super(key: key);

  @override
  _RecCardState createState() => _RecCardState();
}

class _RecCardState extends State<RecCard> {
  bool _show_option = false;
  @override
  Widget build(BuildContext context) {
    TextEditingController _controller = TextEditingController();
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(
        vertical: 12,
        horizontal: 10,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(37.0),
      ),
      child: Container(
        constraints: BoxConstraints.tightForFinite(
          width: double.maxFinite,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(37.0),
          image: DecorationImage(
            image: AssetImage(widget.img),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              widget.title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
              ),
              textAlign: TextAlign.center,
            ),
            Text(
              widget.content,
              style: TextStyle(color: Colors.grey[850], fontSize: 18),
              overflow: TextOverflow.clip,
              textAlign: TextAlign.center,
            ),
            AnimatedCrossFade(
                alignment: Alignment.bottomCenter,
                firstChild: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(37.0),
                    side: BorderSide(
                      color: Colors.grey,
                    ),
                  ),
                  elevation: 5.0,
                  child: Text(
                    widget.text_button,
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      _show_option = true;
                    });
                  },
                ),
                secondChild: Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15.0, 0, 0, 0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Expanded(
                          flex: 4,
                          child: TextField(
                            autocorrect: true,
                            autofocus: false,
                            maxLines: 1,
                            maxLength: 15,
                            controller: _controller,
                            decoration:
                                InputDecoration(hintText: 'Exercise Name'),
                            /* onEditingComplete: () => Navigator.pushNamed(
                                context, '/videoex',
                                arguments: ArgsBeginEx(_controller.text)), */
                          ),
                        ),
                        RawMaterialButton(
                          shape: CircleBorder(),
                          fillColor: Theme.of(context).primaryIconTheme.color,
                          child: Icon(
                            Icons.send,
                            color: Colors.white60,
                            size: 30,
                          ),
                          onPressed: () {
                            _show_option = false;
                            Navigator.pushNamed(context, '/recordex',
                                arguments: ArgsBeginEx(_controller.text));
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                crossFadeState: !_show_option
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: const Duration(seconds: 1)),
          ],
        ),
      ),
    );
  }
}
