import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:path_provider/path_provider.dart';

void transform(dynamic pose) {
  Iterable<dynamic> points = pose["keypoints"].values;
  int len = points.length;
  double xo = points.map((el) => el['x']).reduce((a, b) => a + b) / len;
  double yo = points.map((el) => el['y']).reduce((a, b) => a + b) / len;
  double xmin = 0, ymin = 0, xmax = 0, ymax = 0;
  points.forEach((k) {
    k["x"] -= xo;
    k["y"] -= yo;
    xmin = k["x"] < xmin ? k["x"] : xmin;
    ymin = k["y"] < ymin ? k["y"] : ymin;
    xmax = k["x"] > xmax ? k["x"] : xmax;
    ymax = k["y"] > ymax ? k["y"] : ymax;
  });

  pose["stats"] = {
    "origin": {
      "x": xo,
      "y": yo,
    },
    "min": {
      "x": xmin,
      "y": ymin,
    },
    "max": {
      "x": xmax,
      "y": ymax,
    },
  };
}

void scale(dynamic pose) {
  double xmin = pose["stats"]["min"]["x"],
      ymin = pose["stats"]["min"]["y"],
      xmax = pose["stats"]["max"]["x"],
      ymax = pose["stats"]["max"]["y"];

  double xfrac = xmax - xmin;
  double yfrac = ymax - ymin;

  pose["keypoints"].values.forEach((k) {
    k["x"] = -1 + (((k["x"] - xmin) * 2) / xfrac);
    k["y"] = -1 + (((k["y"] - ymin) * 2) / yfrac);
  });
}

double diffPose(dynamic pose, dynamic res) {
  double diff = 0.0;
  for (var key in pose["keypoints"].keys) {
    diff += (pose["keypoints"][key]["x"] - res["keypoints"][key]["x"]).abs();
    diff += (pose["keypoints"][key]["y"] - res["keypoints"][key]["y"]).abs();
  }
  diff = 1 - (diff / (pose["keypoints"].length * 2));
  return diff;
}

///Can be used to compute a difference score, one argument is a list
///to handle cases where either saved_exercise or results are longer than the
///other, so some sort of split is necessary. Useful also when handling new
///results from addPose(...) since Tflite returns a list either way.
double diffPoses(dynamic pose, List<dynamic> results) {
  //double diff = results.fold(0.0, (val, r) => val + diffPose(pose, r));
  double diff = results.map((r) => diffPose(pose, r)).reduce(max);
  return diff;
}

double window_diffPoses(List<dynamic> poses, List<dynamic> results) {
  double diff = poses.map((pose) => diffPoses(pose, results)).reduce(max);
  return diff;
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

Future<File> _localFile(String file) async {
  final path = await _localPath;
  return File('$path/$file');
}

Future<File> writeToFile(String f, String json) async {
  final file = await _localFile(f);
  print(file.path);
  // Write the file.
  return file.writeAsString(json);
}

Future<List<dynamic>?> readFromFile(String f) async {
  try {
    final file = await _localFile(f);

    // Read the file.
    String contents = await file.readAsString();

    return jsonDecode(contents);
  } catch (e) {
    // If encountering an error, return 0.
    return null;
  }
}
