import 'package:flutter/material.dart';
import 'Components/custom_form.dart';

class AddEntryDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Enter new user info'),
      ),
      body: CustomForm(),
    );
  }
}
