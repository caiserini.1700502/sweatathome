import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as fire;

class Profile {
  String name;
  String surname;
  String gender;
  String username;
  double height;
  double weight;
  List exercises = [];

  Profile(
      {required this.name,
      required this.surname,
      required this.gender,
      required this.username,
      required this.height,
      required this.weight,
      this.exercises = const []});

  static final Profile current = Profile(
      name: "",
      surname: "",
      gender: "",
      username: "",
      height: 0.0,
      weight: 0.0);

  factory Profile.current_user() {
    return current;
  }

  factory Profile.empty() {
    return Profile(
        name: "",
        surname: "",
        gender: "",
        username: "",
        height: 0.0,
        weight: 0.0);
  }

  Profile set_user(
      {required String name,
      required String surname,
      required String gender,
      required String username,
      required double height,
      required double weight}) {
    current.name = name;
    current.surname = surname;
    current.gender = gender;
    current.username = username;
    current.height = height;
    current.weight = weight;
    return current;
  }

  Profile update_user(Profile up) {
    current.name = up.name;
    current.surname = up.surname;
    current.gender = up.gender;
    current.username = up.username;
    current.height = up.height;
    current.weight = up.weight;
    return current;
  }

  Profile.fromJson(Map<String, Object?> json)
      : this(
          name: json['name']! as String,
          surname: json['surname']! as String,
          gender: json['gender']! as String,
          username: json['username']! as String,
          height: json['height']! as double,
          weight: json['weight']! as double,
          exercises: json['exercises']! as List,
        );

  Map<String, Object?> toJson() {
    return {
      "name": name,
      "surname": surname,
      "gender": gender,
      "username": username,
      "height": height,
      "weight": weight,
      "exercises": exercises,
    };
  }

  final userRef = FirebaseFirestore.instance.collection("users").withConverter(
      fromFirestore: (snapshot, _) => Profile.fromJson(snapshot.data()!),
      toFirestore: (user, _) => user.toJson());

  Future<DocumentSnapshot<Profile>> save_user() async {
    await userRef.doc(fire.FirebaseAuth.instance.currentUser!.uid).set(this);
    update_user(this);
    return _load_user();
  }

  Future<DocumentSnapshot<Profile>> _load_user() async {
    return userRef.doc(fire.FirebaseAuth.instance.currentUser!.uid).get();
    /* update_user(firestore_user.data()!);
    return current; */
  }
}

Future<DocumentSnapshot<Profile>> load_user() {
  return Profile.current._load_user();
}

Profile update_user(Profile p) {
  return Profile.current.update_user(p);
}
