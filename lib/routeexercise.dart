import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:sweatathome/argparser.dart';
import 'package:sweatathome/end_exercise.dart';
import 'package:sweatathome/exercise.dart';
import 'package:wakelock/wakelock.dart';

import 'camera.dart';
import 'bndbox.dart';
import 'picovoice.dart' as pcv;

class Routexercise extends StatelessWidget {
  final String exercise;
  final String prefix;
  Routexercise({this.exercise = "", this.prefix = ""});

  @override
  Widget build(BuildContext context) {
    ArgsBeginEx? args =
        ModalRoute.of(context)!.settings.arguments as ArgsBeginEx?;

    return Scaffold(
      appBar: AppBar(
        title: Text('Exercise', style: TextStyle(fontWeight: FontWeight.w600)),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: args != null
          ? _RExercise(args.exname, args.prefix)
          : _RExercise(exercise, prefix),
    );
  }
}

class _RExercise extends StatefulWidget {
  final String exercise;
  final String prefix;
  _RExercise(this.exercise, this.prefix);

  @override
  State<StatefulWidget> createState() {
    return _RExerciseState();
  }
}

class _RExerciseState extends State<_RExercise> {
  List<dynamic> _recognitions = [];
  double score = 1.0;
  // ignore: non_constant_identifier_names
  bool draw_live = false;
  // ignore: non_constant_identifier_names
  int exercise_completed = 0;
  // ignore: non_constant_identifier_names
  int extodo = 5;

  List<double> scores_default = [
    0.61,
    0.72,
    0.83,
    0.47,
    0.97,
  ];

  List<double> scores = [];

  Exercise? exercise;
  //Future<Null> addPose;

  @override
  void initState() {
    super.initState();
    exercise = Exercise(
      widget.prefix,
      widget.exercise,
      setScore,
    );
    pcv.start(callbacks: {"Stop": stop});
    Wakelock.enable();
    Future(_showDialog);
  }

  @override
  void dispose() {
    Wakelock.disable();
    pcv.stop();
    super.dispose();
  }

  Future<void> _showDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('BE CAREFUL!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('Stand in front of the camera and look at it'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text(
                'Ok!',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
              style: TextButton.styleFrom(
                padding: EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
                backgroundColor:
                    Theme.of(context).primaryColor.withOpacity(0.75),
                primary: Colors.blueGrey[400]!.withOpacity(0.75),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    side: BorderSide(color: Colors.black, width: 0.6)),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  setScore(double sc, int numEx) {
    scores.add(sc);
    setState(() {
      score = sc;
      exercise_completed = numEx;
    });
    if (exercise_completed == extodo) {
      pcv.assistantVoice.speak("Finished");
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => EndExercise(scores, widget.exercise)));
    } else {
      pcv.assistantVoice.speak("$exercise_completed");
    }
  }

  String stop() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => EndExercise(
                scores.length > 0 ? scores : scores_default, widget.exercise)));
    return "Ok, i'm stopping";
  }

  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    return Stack(
      children: [
        Camera(
          exercise!.addPose,
        ),
        draw_live
            ? BndBox(
                _recognitions,
                1920,
                1080,
                screen,
              )
            : Container(),
        Positioned(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
            child: Align(
              alignment: Alignment.topRight,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(35),
                      color: Theme.of(context).primaryIconTheme.color,
                    ),
                    padding:
                        EdgeInsets.symmetric(horizontal: 13.0, vertical: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.fitness_center,
                          size: 32.0,
                          color: Colors.white60,
                        ),
                        Text(
                          ": $exercise_completed / $extodo",
                          style: TextStyle(
                            fontSize: 35,
                            fontWeight: FontWeight.bold,
                            color: Colors.white60,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(35),
                      color: Theme.of(context).primaryIconTheme.color,
                    ),
                    padding:
                        EdgeInsets.symmetric(horizontal: 13.0, vertical: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.people,
                          size: 32.0,
                          color: Colors.white60,
                        ),
                        Text(
                          ": 15",
                          style: TextStyle(
                            fontSize: 35,
                            fontWeight: FontWeight.bold,
                            color: Colors.white60,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          child: Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: CircleScore(score: score),
            ),
          ),
        ),
        Positioned(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 22),
              child: RawMaterialButton(
                child: Text(
                  'End',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white60,
                    fontSize: 30,
                  ),
                ),
                padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                fillColor: Theme.of(context).primaryIconTheme.color,
                elevation: 8,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    side: BorderSide(color: Colors.black, width: 0.6)),
                onPressed: () => Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => EndExercise(
                            scores.length > 0 ? scores : scores_default,
                            widget.exercise))),
              ),
            ),
          ),
        )
      ],
    );
  }
}

// ignore: must_be_immutable
class CircleScore extends StatelessWidget {
  CircleScore({
    Key? key,
    required this.score,
  }) : super(key: key);

  double score;
  final ColorTween selColor = ColorTween(
    begin: Colors.red[600],
    end: Colors.lightGreenAccent[700],
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      decoration: BoxDecoration(
        color: selColor.transform(score) /* .withOpacity(0.85) */,
        shape: BoxShape.circle,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            "${(score * 100).round()}",
            style: TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.bold,
              color: Colors.white60,
            ),
          ),
          Text(
            "score",
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.normal,
              color: Colors.white60,
            ),
          ),
        ],
      ),
    );
  }
}
