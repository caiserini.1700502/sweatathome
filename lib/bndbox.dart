import 'package:flutter/material.dart';

class BndBox extends StatelessWidget {
  final List<dynamic> results;
  final double imageH;
  final double imageW;
  final Size screen;

  BndBox(
    this.results,
    this.imageH,
    this.imageW,
    this.screen,
  );

  @override
  Widget build(BuildContext context) {
    List<Widget> _renderKeypoints() {
      var lists = <Widget>[];

      double screenW = screen.width;
      double screenH = screen.height;
      //double factorY = imageH / imageW * screen.width;

      results.forEach((re) {
        var list = re["keypoints"].values.map<Widget>((k) {
          var x = k["x"];
          var y = k["y"];

          if (screenH / screenW > imageH / imageW) {
            var scaleW = screenH / imageH * imageW;
            var scaleH = screenH;
            var difW = (scaleW - screenW) / scaleW;
            x = (x - difW / 2) * scaleW;
            y = y * scaleH;
          } else {
            var scaleW = screenW;
            var scaleH = screenW / imageW * imageH;
            var difH = (scaleH - screenH) / scaleH;
            x = x * scaleW;
            y = (y - difH / 2) * scaleH;
          }

          return Positioned(
            left: x + 100,
            top: y + 65,
            width: 15,
            height: 15,
            child: Container(
              width: 15,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.red,
              ),
            ),
          );
        }).toList();

        lists..addAll(list);
      });

      return lists;
    }

    return Stack(
      children: _renderKeypoints(),
    );
  }
}
