import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

class ScoreWidget extends StatefulWidget {
  double score;

  ScoreWidget(this.score);

  _ScoreState createState() => _ScoreState();
}

class _ScoreState extends State<ScoreWidget>
    with SingleTickerProviderStateMixin {
  Animation<Color?>? colorAnimation;
  AnimationController? controllerAnimation;

  final Duration repeat = Duration(seconds: 1);

  @override
  void initState() {
    super.initState();

    controllerAnimation = AnimationController(duration: repeat, vsync: this);

    colorAnimation = ColorTween(
      begin: Colors.red[600],
      end: Colors.lightGreenAccent[400],
    ).animate(controllerAnimation!)
      ..addListener(() {
        setState(() {});
      });
    controllerAnimation!.animateTo(widget.score);
  }

  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 20,
            child: LinearProgressIndicator(
              backgroundColor: Colors.grey[300],
              valueColor: colorAnimation,
              value: widget.score,
            ),
          ),
          Text('Your score is :${(widget.score * 100).round()}')
        ]);
  }
}
