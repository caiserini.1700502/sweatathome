import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'user.dart' as u;

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(padding: EdgeInsets.fromLTRB(0, 25, 0, 0)),
        Center(
          child: CircleAvatar(
            radius: 55,
            backgroundColor: Colors.grey,
            backgroundImage:
                NetworkImage(FirebaseAuth.instance.currentUser!.photoURL!),
          ),
        ),
        Center(
          child: Text('${u.Profile.current.name} ${u.Profile.current.surname}',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
        ),
        Padding(padding: EdgeInsets.fromLTRB(0, 20.0, 0, 0)),
        Expanded(
          child: ListView(
            children: <Widget>[
              ItemSingle('Info about you:'),
              ItemRow('Gender:', '${u.Profile.current.gender}'),
              ItemRow('Height:', '${u.Profile.current.height} cm'),
              ItemRow('Weight:', '${u.Profile.current.weight} Kg'),
              ItemSingle('Using Swe@Home you did:'),
              ItemRow('Workouts:', '10'),
              ItemRow('Calories:', '150 Kcal'),
              ItemRow('Time:', '10 h'),
            ],
          ),
        )
      ],
    );
  }
}

class DefaultText extends StatelessWidget {
  final String text;
  final double size;
  final FontStyle style;
  final FontWeight weight;
  const DefaultText(
    this.text, {
    Key? key,
    this.size = 20,
    this.style = FontStyle.normal,
    this.weight = FontWeight.normal,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: size,
        fontStyle: style,
        fontWeight: weight,
      ),
    );
  }
}

class ItemRow extends StatelessWidget {
  final String name;
  final String value;
  const ItemRow(
    this.name,
    this.value, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          DefaultText(
            name,
            weight: FontWeight.bold,
          ),
          DefaultText(
            value,
          ),
        ],
      ),
    );
  }
}

class ItemSingle extends StatelessWidget {
  final String name;
  const ItemSingle(
    this.name, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
      child: DefaultText(
        name,
        size: 22,
      ),
    );
  }
}
