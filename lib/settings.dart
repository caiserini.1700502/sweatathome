import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sweatathome/add_entry_dialog.dart';
import 'package:sweatathome/main.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: ListView(
          padding: EdgeInsets.symmetric(
            vertical: 10,
            horizontal: (size.width * 0.15),
          ),
          shrinkWrap: true,
          children: <Widget>[
            PopUpLanguage(),
            Divider(),
            SettingsItem(
              'User info',
              Icons.info,
              () => Navigator.of(context).push(
                new MaterialPageRoute<Null>(
                  builder: (BuildContext context) {
                    return AddEntryDialog();
                  },
                ),
              ),
            ),
            Divider(),
            SettingsItem(
              'Delete exercises', //choose a better description
              Icons.clear, //Icons.clear
              () {
                deleteWorkout();
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(
                    'Deleted all exercises',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ));
              },
            ),
            Divider(),
            SettingsItem(
              "Reset all",
              Icons.replay,
              () => ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(
                'All Resetted',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                ),
              ))),
            )
          ],
        ),
      ),
    );
  }

  deleteWorkout() async {
    for (var f in app_dir!.listSync()) {
      if (f is Directory) {
        continue;
      }
      if (f.path.endsWith('json')) {
        f.deleteSync();
      }
    }
  }
}

Future<int?> buildShowMenu(BuildContext context, TapDownDetails global) {
  return showMenu(
    context: context,
    position: RelativeRect.fromLTRB(
        global.globalPosition.dx, global.globalPosition.dx, 0, 0),
    items: [
      CheckedPopupMenuItem(
        child: Text(
          "English",
          style: TextStyle(color: Colors.black),
        ),
        value: 2,
        checked: true,
      ),
    ],
  );
}

class PopUpLanguage extends StatelessWidget {
  const PopUpLanguage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (TapDownDetails global) => buildShowMenu(context, global),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Theme.of(context).primaryIconTheme.color,
        ),
        child: Column(
          children: <Widget>[
            Icon(Icons.language, color: Colors.white60, size: 40.0),
            Text('Language',
                style: TextStyle(color: Colors.white, fontSize: 20.0))
          ],
        ),
      ),
    );
  }
}

class SettingsItem extends StatelessWidget {
  final Function()? onpressed;
  final IconData icon;
  final String text;
  const SettingsItem(
    this.text,
    this.icon,
    this.onpressed, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: Column(
        children: <Widget>[
          Icon(icon, color: Colors.white60, size: 40.0),
          Text(text, style: TextStyle(color: Colors.white, fontSize: 20.0))
        ],
      ),
      fillColor: Theme.of(context).primaryIconTheme.color,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      elevation: 5,
      onPressed: onpressed,
    );
  }
}
