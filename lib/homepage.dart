import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:sweatathome/register.dart';
import 'package:sweatathome/user.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'homebody.dart';
import 'profile.dart' as p;
import 'settings.dart' as s;
import 'register.dart';

class Homepage extends StatelessWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments
        as Future<DocumentSnapshot<Profile>>;
    return FutureBuilder(
        future: args,
        builder: (ctx, AsyncSnapshot<DocumentSnapshot<Profile>> snapshot) {
          if (snapshot == null) {}
          if (snapshot.connectionState == ConnectionState.done) {
            if (!snapshot.data!.exists) {
              return Register();
            }
            update_user(snapshot.data!.data()!);
            return HomePageLoaded("Swe@Home");
          }
          return Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SpinKitCircle(
                  size: 29.0,
                  duration: Duration(seconds: 2),
                  color: Theme.of(context).accentColor,
                ),
                Text(
                  "Loading user data...",
                  style: TextStyle(fontSize: 18, color: Colors.black),
                ),
              ],
            ),
          );
        });
  }
}

class HomePageLoaded extends StatefulWidget {
  final String title;

  HomePageLoaded(this.title);
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePageLoaded> {
  Widget callPage(int currentIndex) {
    switch (currentIndex) {
      case 0:
        return HomeBody();
      case 1:
        return p.Profile();
      case 2:
        return s.Settings();
      default:
        return HomeBody();
    }
  }

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Text(widget.title, style: TextStyle(fontWeight: FontWeight.w600)),
        centerTitle: true,
      ),
      //resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Image.asset(
                'assets/images/icon.png',
                alignment: Alignment.center,
                fit: BoxFit.contain,
              ),
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text(
                'Home',
                style: TextStyle(fontSize: 20),
              ),
              onTap: () {
                setState(() {
                  _currentIndex = 0;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text(
                'Profile',
                style: TextStyle(fontSize: 20),
              ),
              onTap: () {
                setState(() {
                  _currentIndex = 1;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text(
                'Settings',
                style: TextStyle(fontSize: 20),
              ),
              onTap: () {
                setState(() {
                  _currentIndex = 2;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text(
                'LogOut',
                style: TextStyle(fontSize: 20),
              ),
              onTap: () async {
                await FirebaseAuth.instance.signOut();
                //Navigator.pushNamedAndRemoveUntil(context, "/", (_) => false);
              },
            ),
          ],
        ),
      ),
      body: callPage(_currentIndex),
      bottomNavigationBar: BottomNavigationBar(
          elevation: 5.0,
          currentIndex: _currentIndex,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
          items: [
            BottomNavigationBarItem(icon: new Icon(Icons.home), label: 'Home'),
            BottomNavigationBarItem(
                icon: new Icon(Icons.person), label: 'Profile'),
            BottomNavigationBarItem(
                icon: new Icon(Icons.settings), label: 'Settings')
          ]),
    );
  }
}
