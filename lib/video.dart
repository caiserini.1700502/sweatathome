import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sweatathome/argparser.dart';
import 'package:sweatathome/routeexercise.dart';

import 'package:video_player/video_player.dart';
import 'package:permission_handler/permission_handler.dart';

class StartExercise extends StatefulWidget {
  const StartExercise({Key? key}) : super(key: key);

  @override
  _StartExerciseState createState() => _StartExerciseState();
}

Future<bool> checkVideo(String path) async {
  if (path.contains("assets")) {
    return await rootBundle
        .load(path)
        .then((value) => true, onError: (er, stack) => false);
  } else {
    return File(path).exists();
  }
}

class _StartExerciseState extends State<StartExercise> {
  @override
  Widget build(BuildContext context) {
    ArgsBeginEx args =
        ModalRoute.of(context)!.settings.arguments as ArgsBeginEx;

    [Permission.camera, Permission.microphone].request();

    return FutureBuilder(
      future: checkVideo("${args.prefix}/${args.exname}.mp4"),
      builder: (context, snapshot) {
        print(
            "CHECK VIDEO:  Do we have ${args.prefix}/${args.exname}.mp4 ? ${snapshot.data}");
        if (snapshot.connectionState == ConnectionState.done) {
          print(
              "CHECK VIDEO:  Do we have ${args.prefix}/${args.exname}.mp4 ? ${snapshot.data}");
          if (snapshot.data as bool) {
            return VideoApp(args.exname, args.prefix);
          } else {
            return Routexercise(exercise: args.exname, prefix: args.prefix);
          }
        } else {
          return Container();
        }
      },
    );
  }
}

class VideoApp extends StatefulWidget {
  final String exercise;
  final String prefix;
  VideoApp(this.exercise, this.prefix);
  @override
  _VideoAppState createState() => _VideoAppState();
}

class _VideoAppState extends State<VideoApp> {
  VideoPlayerController? _controller;
  bool already_disposed = false;
  ArgsBeginEx? args;

  @override
  void initState() {
    super.initState();

    _controller = widget.prefix.contains("assets")
        ? _controller = VideoPlayerController.asset(
            '${widget.prefix}/${widget.exercise}.mp4')
        : VideoPlayerController.file(
            File("${widget.prefix}/${widget.exercise}.mp4"));
    _controller!.initialize().then((_) {
      // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
      //_controller.play().then((_) => setState(() {}));
      setState(() {});
    });
    _controller!.setLooping(false);
  }

  @override
  Widget build(BuildContext context) {
    _controller!.addListener(() {
      if (_controller!.value.position == _controller!.value.duration) {
        Navigator.pushNamed(context, '/exercise', arguments: args!);
      }
    });
    return Scaffold(
      appBar: AppBar(
        title: Text('Video', style: TextStyle(fontWeight: FontWeight.w600)),
        centerTitle: true,
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context)),
      ),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 6.0),
                      child: Text(
                        "Hey!",
                        style: TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 45.0),
                      child: Text(
                        "Here is a video to explain the exercise. Watch it if you like or skip it!",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              _controller!.value.isInitialized
                  ? AspectRatio(
                      aspectRatio: _controller!.value.aspectRatio,
                      child: VideoPlayer(_controller!),
                    )
                  : Container(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RawMaterialButton(
                      child: Icon(
                        _controller!.value.isPlaying
                            ? Icons.pause
                            : Icons.play_arrow,
                        size: 60.0,
                        color: Colors.white60,
                      ),
                      fillColor: Theme.of(context).primaryIconTheme.color,
                      shape: CircleBorder(),
                      onPressed: () {
                        setState(() {
                          _controller!.value.isPlaying
                              ? _controller!.pause()
                              : _controller!.play();
                        });
                      },
                    ),
                    Padding(padding: EdgeInsets.symmetric(horizontal: 10.0)),
                    RawMaterialButton(
                      child: Icon(
                        Icons.skip_next,
                        size: 60.0,
                        color: Colors.white60,
                      ),
                      fillColor: Theme.of(context).primaryIconTheme.color,
                      shape: CircleBorder(),
                      onPressed: () {
                        _controller!.pause();
                        /* .then((_) {
                          already_disposed = true;
                          _controller!.dispose();
                        }); */
                        Navigator.pushReplacementNamed(
                          context,
                          '/exercise',
                          arguments: ArgsBeginEx('squat'),
                        );
                      },
                    )
                  ],
                ),
              )
            ]),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller!.dispose();
  }
}
