import 'package:flutter/material.dart';
import 'argparser.dart';
import 'Components/custom_card.dart';
import 'user.dart';

class HomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      body: new HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 10,
            ),
            child: Text(
              'Hello, ${Profile.current.name}',
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 21,
              ),
            ),
          ),
          Expanded(
            child: ExCard(
                img: 'assets/images/workout.png',
                title: 'Start workout',
                content:
                    'Your personal trainer is ready. She\'s waiting for you!',
                text_button: 'START',
                route: '/videoex'),
          ),
          Expanded(
            child: RecCard(
              img: 'assets/images/work3.png',
              title: 'Personalize your workout',
              content: 'Insert your personal workout. ',
              text_button: "INSERT",
              route: '/recordex',
              //args: ArgsBeginEx('squat'),
            ),
          ),
          Expanded(
            child: CustomCard(
                img: 'assets/images/work2.png',
                title: 'Your recent workout',
                content: 'Look how far you have done!',
                text_button: 'WATCH',
                route: '/watchex'),
          ),
        ]);
  }
}
