import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

import 'package:picovoice/picovoice_manager.dart';
import 'package:picovoice/picovoice_error.dart';
import 'package:flutter_tts/flutter_tts.dart';

FlutterTts assistantVoice = FlutterTts();

Future<void> initTTS() async {
  assistantVoice..setLanguage("en-Us");
  assistantVoice..setVolume(0.8);
  assistantVoice..setSpeechRate(0.75);
  assistantVoice..setPitch(0.8);
  assistantVoice..setVoice({"name": "karen", "locale": "en-US"});
}

PicovoiceManager _picovoiceManager = PicovoiceManager.create(
  "assets/assistant/jarvis_android.ppn",
  _wakeWordCallback,
  "assets/assistant/intent.rhn",
  _inferenceCallback,
  porcupineSensitivity: 0.897,
  rhinoSensitivity: 0.75,
);

Map<String, String Function()> _callBacks = {
  "Next": () => "",
  "Recording": () => "",
  "Stop": () => "",
};

Future<String?> start({Map<String, String Function()>? callbacks}) async {
  if (callbacks != null) {
    _callBacks = callbacks;
  }
  try {
    _picovoiceManager.start();
  } on PvError catch (ex) {
    return ex.message;
  }
  return "Picovoice started";
}

void stop() {
  _picovoiceManager.stop();
}

void _wakeWordCallback() {
  print("wake word detected!");
}

void _inferenceCallback(Map<String, dynamic> inference) {
  if (inference['isUnderstood']) {
    //Map<String, String> slots = inference['slots'];
    var intent = inference['intent'];
    if (_callBacks.containsKey(intent)) {
      String message = _callBacks[intent]!();
      assistantVoice.speak(message);
    }
  } else {}
  print(inference);
}
